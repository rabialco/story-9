from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import home, log_in, regist
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# Create your tests here.
class Story9_UnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_using_index_func_1(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_using_index_func_2(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, log_in)
    
    def test_using_index_func_3(self):
        found = self.client.get('/profile/')
        self.assertTemplateUsed(found, 'log/index.html')

    def test_using_index_func_4(self):
        found = self.client.get('/register/')
        self.assertTemplateUsed(found, 'registration/signup.html')
    

    def test_form_register(self):
        response = self.client.post(
            reverse('login_page:register'), data={
        'username': 'andi.muke', 'password1' : 'namasayamarandi', 'password2' : 'namasayamarandi'
        })


        input = User.objects.all().first()

        self.assertEqual(input.username, 'andi.muke')