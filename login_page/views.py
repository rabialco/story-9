from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def home(request):
    return render(request, 'registration/login.html')

def log_in(request):
    if(request.user):
        user = request.user
    return render(request, 'log/index.html', {"logged_in_user" : user})

def regist(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login_page:login')
        # else:
        #     for msg in form.error_messages:
        #         print(form.error_messages[msg])

    form = UserCreationForm
    return render(request, 'registration/signup.html', context={"form":form})