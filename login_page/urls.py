from django.urls import path
from .views import home, log_in, regist
from django.contrib.auth.views import LogoutView

app_name = "login_page"

urlpatterns = [
    path('', home, name = 'login'),
    path('profile/', log_in, name ='profile'),
    path('register/', regist, name='register'),
    path('logout/', LogoutView.as_view(next_page='login'), name = 'logout')
]